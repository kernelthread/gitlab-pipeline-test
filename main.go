package main

import (
	"flag"
	"fmt"
	"log/slog"
	"net/http"
	"sync/atomic"
	"time"
)

var (
	appStartTime string
	visitor      atomic.Int64
	flagAddr     = flag.String("addr", ":8080", "http service address")
)

func init() {
	appStartTime = time.Now().Format("2006-01-02 15:04:05")
}

func main() {
	slog.Default().Handler()
	mux := http.NewServeMux()
	mux.Handle("/", http.HandlerFunc(Index))
	slog.Info("Starting webserver", "address", *flagAddr)
	s := &http.Server{
		Addr:         *flagAddr,
		Handler:      mux,
		ReadTimeout:  10 * time.Second,
		WriteTimeout: 10 * time.Second,
	}
	err := s.ListenAndServe()
	if err != nil {
		slog.Error("Webserver failed", "error", err)
	} else {
		slog.Info("Webserver terminated")
	}
}

const IndexMessage = `Hello World!
You are visitor #%d since %v
`

func Index(w http.ResponseWriter, r *http.Request) {
	slog.Info("Request", "client", r.RemoteAddr, "method", r.Method, "url", r.URL)
	v := visitor.Add(1)
	_, _ = fmt.Fprintf(w, IndexMessage, v, appStartTime)
}
