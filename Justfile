#!/usr/bin/env -S just -f

# just - 🤖 Just a command runner - https://github.com/casey/just
# https://just.systems/man/en/introduction.html

packages     := `go list gitlab.com/kernelthread/gitlab-pipeline-test/...`
package_dirs := `go list -f '{{.Dir}}' gitlab.com/kernelthread/gitlab-pipeline-test/...`

@default: help

# this help
@help:
  just --list

#all: clean test binary container-kaniko
all: clean test

clean:
  rm -rf gitlab-pipeline-test gitlab-pipeline-test.tar dist/

test:
  go test

binary:
  mkdir -p dist
  go build -o dist/gitlab-pipeline-test

run:
  go run gitlab.com/kernelthread/gitlab-pipeline-test

container-buildah:
  buildah build -t gitlab-pipeline-test .

container-kaniko:
  mkdir -p dist
  rm -f dist/gitlab-pipeline-test.tar
  podman run --rm -it \
    -v $(shell pwd):/workspace \
    gcr.io/kaniko-project/executor:debug \
    --context "/workspace" \
    --dockerfile "/workspace/Containerfile" \
    --tar-path "/workspace/dist/gitlab-pipeline-test.tar" \
    --destination gitlab-pipeline-test:latest \
    --no-push

  podman load --input dist/gitlab-pipeline-test.tar

#group('qa')]
# group('qa')]
