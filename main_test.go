package main

import (
	"runtime/debug"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestHello(t *testing.T) {
	info, ok := debug.ReadBuildInfo()
	require.True(t, ok)
	mainModule := info.Main
	assert.Equal(t, "gitlab.com/kernelthread/gitlab-pipeline-test", mainModule.Path)
}
