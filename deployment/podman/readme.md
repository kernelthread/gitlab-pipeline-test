# Resource Definition for Podman's Quadlet

Start:
```bash
podman kube play kube.yaml
```

Stop:
```bash
podman kube down kube.yaml
```
