# Gitlab Pipeline Test

## Features

- Small "hello world" golang web service
- GitLab pipeline
    - Compile golang binary
    - Run unit tests
    - Build container
        - [Kaniko](https://github.com/GoogleContainerTools/kaniko)
          based build to run inside the container of the pipeline builder
        - Publish container to GitLab container registry
    - Deploy via webhook
      - https://bind.ch/gitlab-pipeline-test
