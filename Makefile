packages=$(shell go list gitlab.com/kernelthread/gitlab-pipeline-test/...)
packages_dirs=$(shell go list -f '{{.Dir}}' gitlab.com/kernelthread/gitlab-pipeline-test/...)

.PHONY: all
all: clean test binary container-kaniko

.PHONY: clean
clean:
	rm -rf gitlab-pipeline-test gitlab-pipeline-test.tar dist/

.PHONY: container-buildah
container-buildah:
	buildah build -t gitlab-pipeline-test .

.PHONY: container-kaniko
container-kaniko:
	mkdir -p dist
	rm -f dist/gitlab-pipeline-test.tar
	podman run --rm -it \
	  -v $(shell pwd):/workspace \
	  gcr.io/kaniko-project/executor:debug \
	  --context "/workspace" \
	  --dockerfile "/workspace/Containerfile" \
	  --tar-path "/workspace/dist/gitlab-pipeline-test.tar" \
	  --destination gitlab-pipeline-test:latest \
	  --no-push

	podman load --input dist/gitlab-pipeline-test.tar

.PHONY: run-container
run-container:
	podman run --rm -it -p 8080:8080/tcp localhost/gitlab-pipeline-test:latest

.PHONY: build-and-run
build-and-run: container-kaniko run-container

.PHONY: run
run:
	go run gitlab.com/kernelthread/gitlab-pipeline-test

.PHONY: binary
binary:
	mkdir -p dist
	go build -o dist/gitlab-pipeline-test

.PHONY: test
test:
	go test

.PHONY: push-to-minikube
push-to-minikube:
	rm -f dist/gitlab-pipeline-test.tar
	@echo "Saving image ..."
	podman save --output=dist/gitlab-pipeline-test.tar gitlab-pipeline-test:dev

	@echo "Loading minikube environment ..."
	eval $(minikube docker-env)

	@echo "Loading image into minikube ..."
	docker load --input=gitlab-pipeline-test.tar

.PHONY: src
src:
	@echo fmt ...
	@go fmt $(packages)
	@echo ""

	@echo imports ...
	@goimports -w $(packages_dirs)
	@echo ""

	@echo vet ...
	@go vet $(packages)
	@echo ""

	@echo fix ...
	@go fix $(packages_dirs)
	@echo ""

	@echo staticcheck ...
	@staticcheck -checks=all $(packages)
	@echo ""

	@echo golangci-lint
	@golangci-lint run
	@echo ""

	@echo vulncheck
	@govulncheck $(packages)
	@echo ""

	@echo gosec
	@gosec -terse ./...
	@echo ""

.PHONY: get-dev-deps
get-dev-deps:
	# source code
	go install golang.org/x/tools/cmd/goimports@latest
	go install honnef.co/go/tools/cmd/staticcheck@latest
	go install golang.org/x/vuln/cmd/govulncheck@latest
	go install github.com/golangci/golangci-lint/cmd/golangci-lint@latest
	go install github.com/securego/gosec/v2/cmd/gosec@latest

.PHONY: list-images
list-images:
	skopeo list-tags docker://registry.gitlab.com/kernelthread/gitlab-pipeline-test
